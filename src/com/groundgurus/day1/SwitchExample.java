package com.groundgurus.day1;

public class SwitchExample {
	public static void main(String[] args) {
		String position = "Mid-Level Programmer";
		switch (position) {
		case "Programmer":
		    System.out.println("Teach me master");
		    break;
		case "Mid-Level Programmer":
		    System.out.println("Learning is the key to success");
		    break;
		case "Senior Programmer":
		    System.out.println("Let me show you how to do it");
		    break;
		default:
		    System.out.println("HTML is a programming language");
		}
	}
}
