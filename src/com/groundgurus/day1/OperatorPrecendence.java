package com.groundgurus.day1;

public class OperatorPrecendence {
	public static void main(String[] args) {
		int x = 0; // 1, 2
		
		int y = ++x + x++;
		
		System.out.println("x=" + x);
		System.out.println("y=" + y);
		
		int a = ~10; // 0000 1010, 1111 0101
		System.out.println(a);
		
		int b = 10 >> 2; // 0000 1010, 0000 0010
		System.out.println(b);
		
		
	}
}
