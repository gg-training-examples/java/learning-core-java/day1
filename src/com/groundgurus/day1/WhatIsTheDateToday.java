package com.groundgurus.day1;

import java.text.SimpleDateFormat;
import java.util.Date;

// class - title case
// variables - camel case

public class WhatIsTheDateToday {
	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("yy/dd/MM hh:mm:ss");
		String today = sdf.format(new Date());
		System.out.println(today);
		
		int theBigRedFoxJumpsOverTheLazyDog; // camel case
	}
}
