package com.groundgurus.day1;

public class ArrayExample2 {
	public static void main(String[] args) {
		int x = 10;
		int moreStuff[][] = { {1, 2, 3}, {4, 5, 6}, {7, 8, 9} };
		System.out.println(moreStuff[1][1]);
		
		// Stack memory
		// moreStuff[] = a&123
		// x = 10
		 
		// Heap memory
		// a&123 = {b&456, c&789, d&101112}
		// b&456 = 1, 2, 3
		// c&789 = 4, 5, 6
		// d&101112 = 7, 8, 9
	}
}
