package com.groundgurus.day1;

public class IfExample {
	public static void main(String[] args) {
		String position = "Mid-Level Programmer";
		
		if (position.equals("Programmer")) {
		    System.out.println("Teach me master");
		} else if (position.equals("Mid-Level Programmer")) {
		    System.out.println("Learning is the key to success");
		} else if (position.equals("Senior Programmer")) {
		    System.out.println("Let me show you how to do it");
		} else {
		    System.out.println("HTML is a programming language");
		}
	}
}
