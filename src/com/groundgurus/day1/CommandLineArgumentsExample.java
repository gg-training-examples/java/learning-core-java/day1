package com.groundgurus.day1;

public class CommandLineArgumentsExample {
	public static void main(String[] args) {
		if (args.length > 0) {
			for (String arg : args) {
				System.out.println(arg);
			}
		}
	}
}
