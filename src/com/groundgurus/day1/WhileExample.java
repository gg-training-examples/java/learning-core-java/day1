package com.groundgurus.day1;

import java.util.Random;

public class WhileExample {
	public static void main(String[] args) {
		Random rnd = new Random();
		int randomValue = rnd.nextInt(100) + 1;
		// will continue to loop until value is between 50-59
		while (!(randomValue >= 50 && randomValue <= 59)) {
		    System.out.println(randomValue + " ...not yet");
		    randomValue = rnd.nextInt(100) + 1;
		}
		System.out.println(randomValue + " ...Done!");
	}
}
