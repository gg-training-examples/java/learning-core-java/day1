package com.groundgurus.day1;

public class PrimitiveTypes {
	public static void main(String[] args) {
		int x = 10;  // 32 bits, -2^31 to 2^31 - 1
		byte y = 10; // 8 bits, -2^7 to 2^7 - 1, -128 to 127
		
		byte z = (byte)129;
		
		System.out.println(z);
		
		double myFloat = 10.0;
		
		char c = '\u0196';
		System.out.println(c);
	}
}
