package com.groundgurus.day1;

/**
 * <h1>This is an example of operators</h1>
 * 
 * @author pgutierrez
 */
public class OperatorPrecendence2 {
	public static void main(String[] args) {
		int x = 14 & 20; //   0000 1110 
		                 // & 0001 0100
		                 // -----------
		                 //   0000 0100
		
		System.out.println(x);
		
		int y = 10 ^ 20; //   0000 1010
		                 // ^ 0001 0100
		                 // -----------
		                 //   0001 1110
		System.out.println(y);
		
		int z = 14 | 20; //   0000 1110
        				 // | 0001 0100
                         // -----------
                         //   0001 1110
		System.out.println(z);
		
		String a = y > z ? "yes" : "no";
		
		int b = 10;
		b += 21; // b = b + 21
		System.out.println(b);
		
		int total = 10 + 20 + 30 + 40 + 50;
		String resultMessage = "You have a total of " + (10 + 20 + 30 + 40 + 50);
		System.out.println(resultMessage);
	}
}
