package com.groundgurus.day1;

public class ForExample {
	public static void main(String[] args) {
		String days[] = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };
//		for (int i = 0; i < days.length; i++) {
//			System.out.println(days[i]);
//		}
//		for (int i = days.length - 1; i >= 0; i--) {
//			System.out.println(days[i]);
//		}
		for (String day : days) {
			System.out.println(day);
		}
	}
}
